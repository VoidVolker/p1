USER = "$(shell id -u):$(shell id -g)"

development-setup-env:
	ansible-playbook ansible/development.yml -i ansible/development -vv
